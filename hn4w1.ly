\version "2.20.0"

hnIV_waltzI = \relative c' {
  %\clef bass
  \key c \major 
  \compressFullBarRests
  R2. 
  r4 e\pp e r e e r e e r e e 
  r e e r e e r e e r e e 
  r e e r e e r e e r e e 
  r e e r e e r e e r e e 
  r a, a r a a r a a r a a 
  
  r d d r d d d r r R2. 
  
  r4 d d r d d r cis cis r cis cis 
  fis,2. e' cis4 r cis R2.  
  
  r4 b\p b r b b r b b r b\f b 
  r gis gis r gis gis r gis gis R2. 
  r4 b\p b r b b r b b R2. 
  cis2. cis2 r4 b2 b4 b4 r r b r r R2.*3 b4 r
}

