\version "2.20.0"
#(set-global-staff-size 17)
\paper {
  indent = 0\cm  % add space for instrumentName
  short-indent = 0\cm  % add less space for shortInstrumentName
}



\include "global.ly"
\include "hn3intro.ly"
\include "hn3w1.ly"
\include "hn3w2.ly"
\include "hn3w3.ly"
\include "hn3w4.ly"
\include "hn3w5.ly"
\include "hn3c2.ly"

\header {
  instrument = "Horn 3/Trombone"
}

\bookpart {

  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \intro_hnIII } { \intro_global } >>  
      >>          
    >>
    \header { piece="Introduction" }
  }  
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzI_hnIII } { \waltzI_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 1" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzII_hnIII } { \waltzII_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 2" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzIII_hnIII } { \waltzIII_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 3" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzIV_hnIII } { \waltzIV_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 4" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzV_hnIII } { \waltzV_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 5" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \codaII_hnIII } { \codaII_global } >>  
      >>          
    >>
    \header { piece="Coda 2" }
  }   


}




