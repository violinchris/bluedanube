\version "2.20.0"


tromboneOne = \relative c' { 
  
  \clef bass
  \key g \major 
  
  \compressFullBarRests
  
  R2.*6 | 
  b2.\mf~b4 r8 r4 r8 |
  a2.\pp~a4 r8 r4 r8 |
  d2.\f~d4 r8 r4 r8 |
  d2.\f\>~d4 r8\! r4 r8 | 
  a2.\ppp~a4 r8 r4 r8  | 
  cis2.~cis4 r8 r4 r8 | 
  d2.~d4 r8 r4 r8  | 
  gis,2.~gis  | 
  R2.*4  | 
  a2.-\markup { \dynamic mp \italic cresc. }~ | a~ | a~ | a4\sf r4 r  | 
  R2.*13 | R2.\fermataMarkup 
}

tromboneTwo = \relative c' { 
  
  \clef bass
  \key g \major 
  
  \compressFullBarRests
  
  R2.*6 | 
  gis2.\mf~gis4 r8 r4 r8 |
  g2.\pp~g4 r8 r4 r8 |
  
  a2.\f~a4 r8 r4 r8 |
  a2.\f\>~a4 r8\! r4 r8 |
  
  e2.\ppp~e4 r8 r4 r8 |
  e2.~e4 r8 r4 r8 |
  e2.~e4 r8 r4 r8 |
  e2.( d) |
  
  R2.*4 |
  \tag #'pc {a2.~a~a~a4\sf r4 r   } |
 % \tag #'sl {a2.
          %   ~  | a~  | a~  | a4\sf r4 r  
 % } 
  R2.*13  | R2.\fermataMarkup |
  
  
}






