\version "2.22.1"
pppcresc = \markup { \dynamic ppp \italic cresc. }
\layout {
  \context {
    \Staff
    \consists Measure_counter_engraver
  }
}
hnIV_codaII = \relative c' {
  \override MultiMeasureRest #'expand-limit = 1
  \set Score.restNumberThreshold = #0

  \time 3/4
  \partial 4 r4
    \compressEmptyMeasures
  R2.*4 
    R2.*4 
    e2.\p~e~e4 r4 r
    d r r e2. e4 r r
    R2.*4
    r4 d-.\p d-. r4 d-. d-. 
    r4 d-. d-. r4 d-. d-. 
    r cis-. cis-. r cis-. cis-. 
    r cis-. cis-. r cis-. cis-. 
    r4 d-. d-. r4 d-. d-. 
    r4 d-. d-. r4 d-. d-. 
    a2.\< cis2(->\f b4) 
    r4 cis d cis r4 r4 
    R2.*8 
    c!4\f r4 r c r r 
    r a'8 a a4 a a a gis r4 r
    R2.*3
    
    \startMeasureCount
    r4 g, g r4 g g 
    r4 g g r4 g g 
    r4 g g r4 g g 
    \stopMeasureCount
    r4 g a r a a 
    
    R2.*2
    
    
    r4 a a r a a c r r a2.\<
    R2.*2\!
    
    
    e'2._\markup { \italic cresc. } ~
    e e\f f fis f! e4 r r 
    b'-> r r b-> r r 
    R2.

    \startMeasureCount
    r4 e, e r4 e e 
    r4 e e r4 e e 
    r4 e e r4 e e 
    r4 e e r4 e e 
    r4 e e r4 e e 
    r4 e e r4 e e 
    r4 e\< e r4 e e 
    r4 e e r4 e e 
    \stopMeasureCount
    
    r a,\f a r a a r a a r a a 
    r d d r d d d r r R2. 
    r4 d\ff d r d d r cis cis r cis cis
    fis,2. e'
    \set Score.restNumberThreshold = #1
    R2.^\markup { \bold "G.P." } 
    \set Score.restNumberThreshold = #0
    R2.*26
    
    <<
      { e2.\rest e2.\rest e2.\rest \cueClefUnset }
      \new CueVoice {
        \cueClef "treble" 
        \stemUp b'4^(^"Fl.I" d fis) 
        gis2.\startTrillSpan~
        gis2.\stopTrillSpan
      }
    >>    
    
    %R2.*3

    a,,4-\pppcresc a a a a a 
    a a a a a a 
    \startMeasureCount
    cis\f r r 
    cis r r cis r r cis r r 
    cis r r
    \stopMeasureCount
    cis r4 r8. a16 a2.\fermata
}

\score {
  \hnIV_codaII
}
