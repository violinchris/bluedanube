\version "2.20.0"
#(set-global-staff-size 17)
\paper {
  indent = 0\cm  % add space for instrumentName
  short-indent = 0\cm  % add less space for shortInstrumentName
}



hornTwoWaltzTwo  = \relative c'' {

}
\include "global.ly"
\include "horn.ly"


hornThreeWaltzOne = \relative c'' {

  \key c \major  
  
  \compressFullBarRests
  
  R2. 
  r4 a a r4 a a r4 a a r4 a a |
  r gis gis r gis gis r gis gis r gis gis r gis gis r gis gis r gis gis r gis gis |
  r4 a a r4 a a r4 a a r4 a a |
  r cis, cis r cis cis r cis cis r cis cis |
  r4 a' a r4 a a a r r R2. |
  r4 e\ff e r e e r a a r a a 
  fis2. gis e4 r e R2. 
  
  r4 a\p a r4 a gis r a a r fis\f fis 
  r b, b r b b r b b R2. 
  
  r4 a'4\p a | 
  r a gis | 
  r a a| R2. |
  
  gis2.\f->
  fis2 r4 dis2 dis4 b r r
  
  b r r d'2. b d4 r4 r4 
 
  b,4 r4
  
  
}

hornFourWaltzOne = \relative c' {
  %\clef bass
  \key c \major 
  
  \compressFullBarRests
  
  R2. 
  r4 e\pp e r e e r e e r e e 
  r e e r e e r e e r e e 
  r e e r e e r e e r e e 
  r e e r e e r e e r e e 
  r a, a r a a r a a r a a 
  
  r d d r d d d r r R2. 
  
  r4 d d r d d r cis cis r cis cis 
  fis,2. e' cis4 r cis R2.  
  
  r4 b\p b r b b r b b r b\f b 
  r gis gis r gis gis r gis gis R2. 
  r4 b\p b r b b r b b R2. 
  cis2. cis2 r4 b2 b4 b4 r r b r r R2.*3 b4 r
  
  
}

tromboneOneOne =   \relative c' { 
  
  %\clef bass
 % \key g \major 
  
  \compressFullBarRests
  
  
  \hornThreeWaltzOne
}

tromboneTwoOne = \relative c' { 
  

  
  \compressFullBarRests
  
  \hornFourWaltzOne
}


hnIII_WaltzII = \relative c' { 
    
  \compressFullBarRests
  
}

hnIV_WaltzII = \relative c' { 
  
  \compressFullBarRests
  
  r4
  r4 d d r d d r d d r d d |
  r cis cis r cis cis r cis cis r cis cis |
  r4 d d r d d r d d r d d |
  a2. ais2( b4) r cis d cis r r cis r r cis r4*2
  
  r4 a a r a a r bes bes r bes bes 
  r c c r c c R2.*2 r4 a a r a a 
  R2.*2 a4\pp r r d r r a r r R2.*1 
  
  
}



tbIII_WaltzII = \relative c' { 
  
  
  \compressFullBarRests
  \hnIII_WaltzII
  
}

tbIV_WaltzII = \transpose c f, \relative c { 
  
  %\clef treble
  \key a \major 
  \clef bass
 
   \hnIV_WaltzII
  
}

\bookpart {

  \score  {
    \keepWithTag #'pc
    \removeWithTag #'sl
    <<
      %{
      \new StaffGroup = "StaffGroup_woodwinds" <<
        \new Staff = "Staff_flute" \with { instrumentName = "Flute" } \partcombine \fluteMusic \fluteMusic
  
        \new Staff = "Staff_clarinet" \with {
          instrumentName = \markup { \concat { "Clarinet in B" \flat } }
        }
  
        % Declare that written Middle C in the music
        % to follow sounds a concert B flat, for
        % output using sounded pitches such as MIDI.
        %\transposition bes
  
        % Print music for a B-flat clarinet
        \transpose bes c' \clarinetMusic
      >>
      %}
      %{
      \new StaffGroup = "StaffGroup_brass" <<
        \new Staff = "Staff_hornI" \with { instrumentName = "Horn in F" }
         % \transposition f
          \transpose f c' \hornMusic
  
        \new Staff = "Staff_trumpet" \with { instrumentName = "Trumpet in  C" }
        \trumpetMusic
      >>
      %}
      %{
      \new RhythmicStaff = "RhythmicStaff_percussion"
      \with { instrumentName = "Percussion" }
      <<
        \percussionMusic
      >>
      %}
      %{
      \new PianoStaff \with { instrumentName = "Piano" }
      <<
        \new Staff { \pianoRHMusic }
        \new Staff { \pianoLHMusic }
      >>
      %}
      %{
      \new ChoirStaff = "ChoirStaff_choir" <<
        \new Staff = "Staff_soprano" \with { instrumentName = "Soprano" }
        \new Voice = "soprano"
        \sopranoMusic
  
        \new Lyrics \lyricsto "soprano" { \sopranoLyrics }
        \new GrandStaff = "GrandStaff_altos"
        \with { \accepts Lyrics } <<
          \new Staff = "Staff_altoI"  \with { instrumentName = "Alto I" }
          \new Voice = "altoI"
          \altoIMusic
  
          \new Lyrics \lyricsto "altoI" { \altoILyrics }
          \new Staff = "Staff_altoII" \with { instrumentName = "Alto II" }
          \new Voice = "altoII"
          \altoIIMusic
  
          \new Lyrics \lyricsto "altoII" { \altoIILyrics }
        >>
  
        \new Staff = "Staff_tenor" \with { instrumentName = "Tenor" }
          \new Voice = "tenor"
          \tenorMusic
  
        \new Lyrics \lyricsto "tenor" { \tenorLyrics }
      >>
  
      %}
      
      \new StaffGroup = "StaffGroup_strings" <<
        
        %{
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" \with { instrumentName = "Horn I" }
          \violinIMusic
  
          \new Staff = "Staff_violinII" \with { instrumentName = "Horn II" }
          \violinIIMusic
        >>
        %}
        %{
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" \with { instrumentName = "Trumpet I" }
          \violinIMusic
  
          \new Staff = "Staff_violinII" \with { instrumentName = "Trumpet II" }
          \violinIIMusic
        >>      
        %}
  
          \new Staff = "Staff_violinI" \with { instrumentName = "" }
          <<
             { \globalIntro }
            {       
              \partcombine  \tromboneOne  \tromboneTwo 
            }
          >>
      >>    
      
      %{
      \new StaffGroup = "StaffGroup_strings" <<
        \new GrandStaff = "GrandStaff_violins" <<
          \new Staff = "Staff_violinI" \with { instrumentName = "Violin I" }
          \violinIMusic
  
          \new Staff = "Staff_violinII" \with { instrumentName = "Violin II" }
          \violinIIMusic
        >>
  
        \new Staff = "Staff_viola" \with { instrumentName = "Viola" }
        \violaMusic
  
        \new Staff = "Staff_cello" \with { instrumentName = "Cello" }
        \celloMusic
  
        \new Staff = "Staff_bass" \with { instrumentName = "Double Bass" }
        \bassMusic
      >>
      %}
      
    >>
    \header { 
      piece="Intro "
    }
    \layout { }
  }

  %{
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \partcombine  \tromboneOneOne  \tromboneTwoOne } { \globalWaltzOne } >>  
      >>          
    >>
    \header { 
      piece="Waltz No 1 "
    }
    \layout { 
    }
  }
  %}
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \partcombine  \tbIII_WaltzII  \tbIV_WaltzII } { \globalWaltzTwo } >>  
      >>          
    >>
    \header { 
      piece="Waltz No 2 "
    }
    \layout { 
    }
  }  
  

}




