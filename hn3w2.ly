\version "2.22.1"

wIItbIII = \relative c'' {
  \time 3/4
  \partial 4 r4 
  
  
  r4 gis\p gis r4 gis gis 
  r e e r e e r e e r e e r e e r e e 
  r4 gis gis r4 gis gis 
  r e e r e e 
  R2.
  cis'2(\f-> b4) 
  r e,\f gis 
  e r r 
  e r r
  e r s 
  
  r f\p f r f f 
  r g g r g g 
  r bes bes r bes bes
  R2.*2
  
  r4 f f r f f d2.~d e4\pp r r f r r e r r R2.
}

\score {
  \wIItbIII
}
