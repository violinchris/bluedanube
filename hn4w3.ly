\version "2.20.0"

hnIV_waltzIII = \relative c' { 
  
  \compressFullBarRests
  
  r4
  r4 d d r d d r d d r d d |
  r cis cis r cis cis r cis cis r cis cis |
  r4 d d r d d r d d r d d |
  a2. ais2( b4) r cis d cis r r cis r r cis r4*2
  
  r4 a a r a a r bes bes r bes bes 
  r c c r c c R2.*2 r4 a a r a a 
  R2.*2 a4\pp r r d r r a r r R2.*1 
  
  
}
