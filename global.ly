mpcresc = \markup { \dynamic mp \italic cresc. }

\version "2.20.0"
intro_global = {
  \time 6/8 s2.*22 \bar "||"
  \time 3/4 s2.*22 \bar "||"
}

markright = {
      \once \override Score.RehearsalMark #'self-alignment-X = #LEFT % Aligning to the Left
}
ur = { % mark to the upper right
    \once \override Score.RehearsalMark #'self-alignment-X = #RIGHT % Aligning to the Right
    \once \override Score.RehearsalMark #'break-visibility = #begin-of-line-invisible % Even at the end of the line
}
lr = { % mark to the lower right
    \ur % setting it to the upper right first
    \once \override Score.RehearsalMark #'direction = #DOWN % lowering the mark
}

lrh = { % mark to the lower right
    \once \override Score.RehearsalMark #'direction = #DOWN % lowering the mark
}

% ====================
% = segnos and codas =
% ====================
segno   = \mark \markup { \tiny \musicglyph #"scripts.segno" } % segno as a mark
segnot  =      ^\markup { \musicglyph #"scripts.segno" } % segno as a Text incase of a rehearsal mark
coda    = \mark \markup { \musicglyph #"scripts.coda" }  % coda as a mark
codat   =      ^\markup { \musicglyph #"scripts.coda" }  % coda as a Text incase of a rehearsal mark


% ===========================================
% = helper script for marking D.S. and D.C. =
% ===========================================
dsh   = \mark \markup{ \bold \tiny   "D.S."} % D.S. Helper
dch   = \mark \markup{ \bold   "D.C."} % D.C. Helper
dsafh = \mark \markup{ \italic "D.S. al Fine"} % D.S. Al Fine Helper
dcafh = \mark \markup{ \italic "D.C. al Fine"} % D.C. Al Fine Helper
dsl  = { \lr \dsh } % D.S. in the Lower right
dsu  = { \ur \dsh } % D.S. in the Upper right
dcl  = { \lr \dch } % D.C. in the Lower right
dcu  = { \ur \dch } % D.C. in the Upper right

waltzI_global = {
  
    \time 3/4 s2.* 32 \bar "||" s2. 
   \bar ".|:"
    s2.*15
    \set Score.repeatCommands = #'((volta "1."))
    s2.
    \set Score.repeatCommands = #'((volta "2.") end-repeat)
    s2.*4
    \set Score.repeatCommands = #'((volta "Fine") end-repeat)
    
    %\once \override Score.RehearsalMark.outside-staff-priority = #0000
    \mark \markup { \small { \musicglyph "scripts.segno" } }
    
      %\override Staff.BreathingSign.text = \markup { \musicglyph "scripts.segno" }
    %\breathe
    s2
    \set Score.repeatCommands = #'((volta #f))
    \bar "||"
}


waltzII_global = {
  
    \time 3/4 
    \partial 4 s4 

    s2.*15
    \set Score.repeatCommands = #'((volta "1."))
    s2.
    \set Score.repeatCommands = #'((volta "2.") end-repeat)
    s2.
    \set Score.repeatCommands = #'((volta "Fine."))
    \bar "||"
    s2.
    \set Score.repeatCommands = #'((volta #f))
    \bar "||"
    s2.*16 
    \ur \dsafh
    \bar "||"
}

waltzIII_global = {
  
    \time 3/4 
    \partial 4 s4
    \segno
    \repeat volta 2 {
      s2.*15
    }
    \alternative {
      { s2. }
      { s2.  }    
    }   
    \markright  \mark \markup { \bold \small "Lebhaft" }
    \bar ".|:"
    s2.*15
    \set Score.repeatCommands = #'((volta "1."))
    s2.
    \set Score.repeatCommands = #'((volta "2.") end-repeat)
    s2.
    \dsl
    \set Score.repeatCommands = #'((volta "Fine."))
    \bar "||"
    s2.
    \set Score.repeatCommands = #'((volta #f))
    \bar "||"        
    
    
}
\score { \waltzIII_global }
waltzIV_global = {
  
    \time 3/4 
    s2.*4 \segno
    \repeat volta 2 {
      s2.*12
    }
    \alternative {
      { s2.*4 }
      { s2.*4 }
    }
    
    s2.*15
    
    \set Score.repeatCommands = #'((volta "1."))
    s2.
    \set Score.repeatCommands = #'((volta "2.") end-repeat)
    s2.
    \set Score.repeatCommands = #'((volta "Fine."))
    \bar "||"
    s2.
    \set Score.repeatCommands = #'((volta #f))
    \bar "||"    
    
    %\repeat volta 2 {
      
    %  s2.*16 \lr \dsh
     % \mark \default 
     % \ur \mark \markup { \segno \small { Fine } }
   % }
    
}

%\once \override Score.RehearsalMark
%\score { \waltzIV_global }
waltzV_global = {
    \time 3/4 
    \bar "||"
    s2.*10 \bar "||"
    s2. \segno
    \repeat volta 2 {
      s2.*12
    }
    \alternative {
      { s2.*4 }
      { s2.*4 }
    }
    \bar "||"
    s2.*30
    \lrh \coda
    \set Score.repeatCommands = #'((volta "1."))
    s2.*2
    \bar "||"
    \lr \mark \markup{ \bold   "D.S."}
    \set Score.repeatCommands = #'((volta "Fine"))
    s2.*2
    \set Score.repeatCommands = #'((volta #f))
    \bar "||"
}



codaI_global = {
  
    \time 3/4 
    \bar "||"
}

codaII_global = {
  
    \time 3/4 
    \partial 4 s4 
    s2.*18 \bar "||"
    s2.*32 \bar "||"
    s2.*25 \bar "||"
    

    s2.*21
    s2. % GP
    s2.*40 \bar "|."
    
    
}