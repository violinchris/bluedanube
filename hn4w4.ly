\version "2.20.0"

hnIV_waltzIV = \relative c { 
  
  \compressFullBarRests
  \time 3/4
  f2.\f-> f-> f4-> g8 g g4 R2.
  r4 g\pp g r g g 
  r g g r g g 
  r g g r g g 
  r g a r g g 
  R2.*2
  r4 a a r a a g-.\p r r d'-. r r r e e e r r 
  c-.\p r r
  
  
}

\score {
\hnIV_waltzIV
}