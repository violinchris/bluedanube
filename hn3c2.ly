\version "2.22.1"
pppcresc = \markup { \dynamic ppp \italic cresc. }
\layout {
  \context {
    \Staff
    \consists Measure_counter_engraver
  }
}
hnIII_codaII = \relative c'' {
  %\override MultiMeasureRest #'expand-limit = 1
  \set Score.restNumberThreshold = #0

  \time 3/4
  \partial 4 r4
    \compressEmptyMeasures
  R2.*4 
    R2.*4 
    b2.\p~b~b4 r4 r
    gis r r r gis a gis r r 
    R2.*4
    r4 gis-. gis-. r4 gis-. gis-. 
    r e-. e-. r e-. e-. 
    r e-. e-. r e-. e-. 
    r e-. e-. r e-. e-. 
    r gis-. gis-. r gis-. gis-. 
    r e-. e-. r e-. e-. 
    R2.*1 | 
    cis2(-> b4) 
    r4 e\f gis e r4 r4 | 
    R2.*4 
    e,2.\p~e~e a4 r r f'\f r r |
    f r r r c'8 c c4 dis dis dis e r r 
    R2.*3
    
    r4 c,\p c r c c r 
    b b r b b r 
    b b r b b 
    r c c r c c 
    
    R2.*2
    r4 d d r d d c r r a2.\<
    R2.*2\!
    gis'2._\markup { \italic cresc. } 
    a e\f f fis f! e4 r r 
    d-> r r d-> r r 
    R2.
    r4 a'\p a r4 a a r4 a a r4 a a 
    \startMeasureCount
    r gis gis r gis gis r gis gis r gis gis 
    r gis gis r gis gis r gis gis r gis gis 
    \stopMeasureCount
    r4 a\< a r4 a a r4 a a r4 a a 
    r cis,\f cis r cis cis r cis cis r cis cis 
    r a' a r a a a r r R2.
    r4 e\ff e r e e r a a r a a 
    fis2.-> gis-> 
    \set Score.restNumberThreshold = #1
    R2.^\markup { \bold "G.P." } 
    \set Score.restNumberThreshold = #0
    R2.
    
    cis,8\p r r4 r4
    d8 r r4 r4
    e8 r r4 r4
    cis8 r r4 r4
    d8 r r4 r4
    d8 r r4 r4
    d8 r r4 r4
    d8 r r4 r4
    d8 r r4 r4
    d8 r r4 r4
    d8 r r4 r4
    d8 r r4 r4
    cis8 r r4 r4
    
    R2.*12
    R2.*3
    
    e4-\pppcresc e e e e e e e e e e e 
    \startMeasureCount
    a\f r r a r r a r r a r r a r r
    \stopMeasureCount
    a r r8. a16 a2.\fermata
    
}

\score {
  \hnIII_codaII
}
