%\version "2.20"

hnIII_waltzV = \relative c'' {
  \time 3/4
  e2. e2 e4
  gis, r4 r4 R2. 
  gis2.\p~gis4 r4 r 
  r gis\f gis gis r4 r 
  R2.*3
  r4 e e r4 e e r4 e e r4 e e 
  r4 e e r4 e e r4 e e r4 e e 
  r4 a a r4 a a 
  r4 e e r4 e e 
  
  r4 dis dis dis r r b b b R2.
  
  fis'4 r r ais r r fis b8 b b4 b b b 
  
  r gis gis r gis gis 
  r gis gis r gis gis 
  r gis gis r gis gis 
  r cis, cis r cis cis 
  r fis fis r fis fis 
  r gis gis r gis gis 
  r a a r a a 
  gis b, cis dis e fis 
  r gis gis r gis gis 
  r gis gis r gis gis 
  r gis gis r gis gis 
  r cis, cis r cis cis 
  r fis fis r fis fis 
  b2.~b4 r r a r r fis r r 
  e e e R2. e4 e e e r4
  
}

\score { \hnIII_waltzV }
