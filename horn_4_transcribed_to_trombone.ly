\version "2.20.0"
#(set-global-staff-size 17)
\paper {
  indent = 0\cm  % add space for instrumentName
  short-indent = 0\cm  % add less space for shortInstrumentName
}

\header {
  instrument = "Horn 4/Trombone"
}


\include "global.ly"
\include "hn4intro.ly"
\include "hn4w1.ly"
\include "hn4w2.ly"
\include "hn4w3.ly"
\include "hn4w4.ly"
\include "hn4w5.ly"
\include "hn4c2.ly"


\bookpart {

  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \intro_hnIV } { \intro_global } >>  
      >>          
    >>
    \header { piece="Introduction" }
  }  
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzI_hnIV } { \waltzI_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 1" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzII_hnIV } { \waltzII_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 2" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzIII_hnIV } { \waltzIII_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 3" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzIV_hnIV } { \waltzIV_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 4" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \waltzV_hnIV } { \waltzV_global } >>  
      >>          
    >>
    \header { piece="Waltz No. 5" }
  } 
  
  \score  {
    \keepWithTag #'pc
    <<
      \new StaffGroup = "StaffGroup_strings" <<  
          \new Staff = "Staff_violinI"
          << { \codaII_hnIV } { \codaII_global } >>  
      >>          
    >>
    \header { piece="Coda 2" }
  }   


}




