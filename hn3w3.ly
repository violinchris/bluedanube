%\version "2.22.1"

waltzIII_hnIII = \relative c' {
  \time 3/4
  \partial 4 r4 
  
  
  r4 d\p d r d d r d d r d d r d d r d d
  r4 cis cis r cis cis r e e 
  r e e r d d r d d R2.*4
  
  r4 g g r fis fis 
  r4 g g r fis fis 
  r4 g g r fis fis 
  r4 g g r fis fis 
  r4 g g r fis fis 
  r4 g g r fis fis 
  r4 g g d\f r r r a' a a r4 r
}

\score { \waltzIII_hnIII }
