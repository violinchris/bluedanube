\version "2.22.1"
\include "global.ly"
intro_hnIII = \relative c' { 
  
  %\clef bass
  %\key d \major 
  \time 3/4
  
  \compressEmptyMeasures
  
  R2.*6 | 
  fis2.\mf~fis4 r8 r4 r8 |
  e2.\pp~e4 r8 r4 r8 |
  a2.\f~a4 r8 r4 r8 |
  a2.\f\>~a4 r8\! r4 r8 | 
  e2.\ppp~e4 r8 r4 r8  | 
  gis2.~gis4 r8 r4 r8 | 
  a2.~a4 r8 r4 r8  | 
  dis,2.~dis  | 
  R2.*4  | 
  e2.-\markup { \dynamic mp \italic cresc. }~ | e~ | e~ | e4\sf r4 r  | 
  R2.*13 | R2.\fermata
}
%{
\score  {
  \new Staff { << \introHnIII \globalIntro >> }
  \header { piece="Intro "}
}  
%}