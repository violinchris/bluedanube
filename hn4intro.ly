\version "2.22.1"
\include "global.ly"
hnIV_intro = \relative c' { 
  
  %\clef bass
  %\key d \major 
  \time 3/4
  \compressEmptyMeasures
  R2.*6 | 
  dis2.\mf~dis4 r8 r4 r8 |
  d2.\pp~d4 r8 r4 r8 |
  
  e2.\f~e4 r8 r4 r8 |
  e2.\f\>~e4 r8\! r4 r8 |
  
  b2.\ppp~b4 r8 r4 r8 |
  b2.~b4 r8 r4 r8 |
  b2.~b4 r8 r4 r8 |
  b2.( a) |
  
  R2.*4 |
  \tag #'pc {e2._\mpcresc~e~e~e4\sf r4 r   } |
 % \tag #'sl {a2.
          %   ~  | a~  | a~  | a4\sf r4 r  
 % } 
  R2.*13  | R2.\fermata |
}

\score  {
  \new Staff { << \hnIV_intro \globalIntro >> }
  \header { piece="Intro "}
}  
