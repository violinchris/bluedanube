\version "2.22.1"

wIVtbIII = \relative c' {
  \time 3/4
  \partial 4 r4 
  
  
  f2. f f4 g,8 g g4 R2. 
  r4 c c r4 c c 
  r b b r b b r b b r b b 
  r c c r c c R2.*2 
  r4 d d r d d 
  
  c-.\p r r g'-. r r r g g g r r 
  c,-.\p r r 
  a2.\< 
  b4\! r r 
  g'8\fz r8 r4 r4
  
  r4 g\f g r g g r g g r g g 
  r f f r f f r f f f r r 
  r g g r g g r g g r g g 
  d2.~d4 d d r g g g r r
}

\score {
  \wIVtbIII
}
