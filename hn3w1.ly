\version "2.20"

hnIII_waltzI = \relative c'' {
  \key c \major  
  \time 3/4
  \compressFullBarRests
  
  R2. 
  r4 a a r4 a a r4 a a r4 a a |
  r gis gis r gis gis r gis gis r gis gis r gis gis r gis gis r gis gis r gis gis |
  r4 a a r4 a a r4 a a r4 a a |
  r cis, cis r cis cis r cis cis r cis cis |
  r4 a' a r4 a a a r r R2. |
  r4 e\ff e r e e r a a r a a 
  fis2. gis e4 r e R2. 
  
  r4 a\p a r4 a gis r a a r fis\f fis 
  r b, b r b b r b b R2. 
  
  r4 a'4\p a | 
  r a gis | 
  r a a| R2. |
  
  gis2.\f->
  fis2 r4 dis2 dis4 b r r
  
  b r r d'2. b d4 r4 r4 
 
  b,4 r4
  
}

\score {
  \hnIII_waltzI
}
